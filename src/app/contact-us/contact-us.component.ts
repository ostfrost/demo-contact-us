import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactUsService } from '../contact-us.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  contactUsForm: FormGroup;
  sent = false;
  error: string = null;

  private formSubmitted = false;

  constructor(private fb: FormBuilder, private cus: ContactUsService) {}

  ngOnInit() {
    this.contactUsForm = this.fb.group({
      to: ['', [Validators.required,  Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
      subject: ['', Validators.required],
      body: ['', Validators.required]
    });
  }

  sendForm() {
    if (this.contactUsForm.valid) {
      this.cus.sendContactUsForm(this.contactUsForm.value)
        .subscribe(
          () => {
            this.sent = true;
            this.contactUsForm.reset();
            this.formSubmitted = false;
          },
          (response) => {
            if (response && response.error && response.error.ResponseStatus && response.error.ResponseStatus.Message) {
              this.error = response.error.ResponseStatus.Message;
            } else {
              this.error = 'Something went wrong. Please contact the support!';
            }
          }
        );
    }

    this.formSubmitted = true;

    return false; // prevent default submit event
  }

  private isValid(control: AbstractControl) {
    return (control.touched || this.formSubmitted) && !control.valid;
  }

  get toHasErrors() {
    return this.isValid(this.contactUsForm.get('to'));
  }

  get subjectHasErrors() {
    return this.isValid(this.contactUsForm.get('subject'));
  }

  get bodyHasErrors() {
    return this.isValid(this.contactUsForm.get('body'));
  }
}
