export class ContactUsRequestData {
  to: string;
  subject: string;
  body: string;
}
