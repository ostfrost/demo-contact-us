import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ContactUsRequestData } from './contact-us-request-data';

@Injectable({
  providedIn: 'root'
})
export class ContactUsService {
  constructor(private http: HttpClient) {}

  sendContactUsForm(data: ContactUsRequestData) {
    const url = 'https://test.secureprivacy.ai/api/email';

    return this.http.post(url, data);
  }
}
